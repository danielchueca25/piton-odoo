# -*- coding: utf-8 -*-
# from odoo import models, fields, api
from odoo import models, fields


class Patients(models.Model):
    _name = 'hospital.hospitaldce_pacient'
    _description = 'Pacientes lista'

    paciente_name = fields.Char(string='Name', required=True)
    paciente_surname = fields.Char(string='Surname', required=True)
    paciente_dni = fields.Char(string='DNI', required=True)
    genre = fields.Selection([
            ('male', 'Male'),
            ('female', 'Female'),
            ('other', 'Other')
        ], string='Genero', default='male')

    adress = fields.Char(string='Direccion', required=True)
    date_bday = fields.Date(string='Fecha de nacimiento', required=True, default=fields.datetime.now())
    nationality = fields.Many2one('res.country', string='nacionality', required=True,
        default=lambda self: self.env['res.country'].search([]))

    ingreso = fields.Boolean(string='Ingreso', required=True, default=False)
    date_ingreso = fields.Date(string='Fecha de ingreso')

    info = fields.Text(string='Informacion')
    seguro = fields.Char(string='Seguro')




    # paciente_age = fields.Integer(string='Age')
    # notes = fields.Text(string='Notes')
    # image = fields.Binary(string='Image')




#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
