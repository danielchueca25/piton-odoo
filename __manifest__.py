# -*- coding: utf-8 -*-
{
    'name': "Hospital DCE",
    'author': "Daniel Chueca Esteruelas",
    'website': "https://github.com/danielchueca/hospital",
    'category': 'Odoo + Piton - M10',
    'version': '1.0',
    'summary': ' creacion de un modulo de odoo con python.'
               ' M10: Sistemas de Gestion Empresarial.'
               ' Curso 2021/20221',
    'sequence': '10',
    'depends': ['base'],
    'data': [
        'views/views.xml',
        'views/templates.xml',
        'data/patient.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'instalable': True,
    'application': True,
    'auto_install': False,
}
